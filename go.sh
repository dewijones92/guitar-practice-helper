#!/bin/bash
set -x;
file="E Phrygian Guitar Lick-cnDItvRfuDE.mp4";
seq 0.3 0.02 1 | xargs -t -I{} mpv --start=0:05 --length=5 --speed={} --geometry="100%:0%" "$file"
